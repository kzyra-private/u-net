import os 
import cv2 
import secrets
import skimage.io
import numpy as np
from copy import deepcopy
from google.colab import drive
import matplotlib.pyplot as plt
from google.colab.patches import cv2_imshow
from sklearn.model_selection import train_test_split

import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import plot_model
from tensorflow.keras.initializers import GlorotUniform, HeNormal
from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau, History

from keras import Input
from keras.models import Model
from keras.layers.merge import concatenate
from keras.callbacks import ModelCheckpoint
from keras.layers.pooling import MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator

from keras.layers.convolutional import Conv2D, Conv2DTranspose
from keras.layers import BatchNormalization, Activation, Dropout

drive.mount("/content/gdrive")


def load_data():
    # Paths to original images
    original_images_path = 'gdrive/MyDrive/U-Net/Images'
    original_masks_path = 'gdrive/MyDrive/U-Net/Masks'

    original_images = os.listdir(original_images_path)
    original_masks = os.listdir(original_masks_path)

    print('Images: ' + str(len(original_images)))
    print('Masks: ' + str(len(original_masks)))

    return [original_images, original_masks]


def resize_data(images_names, masks_names, new_h, new_w):
    desired_size = (new_w, new_h)

    # Paths to original images
    original_images_path = 'gdrive/MyDrive/U-Net/Images/'
    original_masks_path = 'gdrive/MyDrive/U-Net/Masks/'

    # Paths to resized images
    resized_images_path = 'gdrive/MyDrive/U-Net/Resized-images/Vertebra/'
    resized_masks_path = 'gdrive/MyDrive/U-Net/Resized-masks/Vertebra/'

    images = os.listdir(resized_images_path)
    masks = os.listdir(resized_masks_path)

    # Resizing images
    if (len(images) == 0):
        for name in images_names:
            image = cv2.imread(filename=os.path.join(original_images_path, name), flags=cv2.IMREAD_GRAYSCALE)
            image = cv2.resize(src=image, dsize=desired_size)
            cv2.imwrite(filename=os.path.join(resized_images_path, name), img=image)

    # Resizing masks
    if (len(masks) == 0):
        for name in masks_names:
            mask = cv2.imread(filename=os.path.join(original_masks_path, name), flags=cv2.IMREAD_GRAYSCALE)
            mask = cv2.resize(src=mask, dsize=desired_size)
            cv2.imwrite(filename=os.path.join(resized_masks_path, name), img=mask)


def prepare_data(dataset_size, split_size=None):
    if split_size is None:
        split_size = 0.2

    # Paths to images after augmentation
    augmented_images_path = 'gdrive/MyDrive/U-Net/Augmented-images/'
    augmented_masks_path = 'gdrive/MyDrive/U-Net/Augmented-masks/'

    image_names = os.listdir(augmented_images_path)
    mask_names = os.listdir(augmented_masks_path)

    images = []
    masks = []

    cnt=0
    for name in image_names:
        if (cnt < dataset_size):
            temp_image = cv2.imread(os.path.join(augmented_images_path, name), cv2.IMREAD_GRAYSCALE)
            images.append(temp_image)  
            cnt = cnt + 1

    cnt=0
    for name in mask_names:
        if (cnt < dataset_size):
            temp_mask = cv2.imread(os.path.join(augmented_masks_path, name), cv2.IMREAD_GRAYSCALE)
            masks.append(temp_mask)
            cnt = cnt + 1

    images = np.array(images)
    masks = np.array(masks)

    train_images, test_images, train_masks, test_masks = train_test_split(images, masks, test_size=split_size, random_state=42, shuffle=True)

    train_shape = (train_images.shape[0], train_images.shape[1], train_images.shape[2], 1)
    test_shape = (test_masks.shape[0], test_masks.shape[1], test_masks.shape[2], 1)

    train_images = train_images.reshape(train_shape)
    train_masks = train_masks.reshape(train_shape)
    test_images = test_images.reshape(test_shape)
    test_masks = test_masks.reshape(test_shape)

    train_images = train_images / 255
    test_images = test_images / 255
    train_masks = train_masks / 255
    test_masks = test_masks / 255

    train_masks = np.round(train_masks, 0)	
    test_masks = np.round(test_masks, 0)	

    return [train_images, test_images, train_masks, test_masks]


def augment_data(h, w):
    # Path to resized images 
    resized_images_path = 'gdrive/MyDrive/U-Net/Resized-images'
    resized_masks_path = 'gdrive/MyDrive/U-Net/Resized-masks'

    # Paths to images after augmentation
    augmented_images_path = 'gdrive/MyDrive/U-Net/Augmented-images'
    augmented_masks_path = 'gdrive/MyDrive/U-Net/Augmented-masks'

    augmented_images = os.listdir(augmented_images_path)
    augmented_masks = os.listdir(augmented_masks_path)

    generator = ImageDataGenerator(
        rotation_range=25,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        vertical_flip=True,
    )

    seed = 300392 
    images_number = 900
    batch_size = 1

    # Generating more images and masks
    if (len(augmented_images) == 0 and len(augmented_masks) == 0):
        cnt = 0
        for batch in generator.flow_from_directory(directory=resized_images_path, target_size=(h,w), color_mode='grayscale', batch_size=batch_size, shuffle=True, seed=seed, save_to_dir=augmented_images_path):
            cnt = cnt + 1
            if cnt == images_number:
                break

        cnt = 0
        for batch in generator.flow_from_directory(directory=resized_masks_path, target_size=(h,w), color_mode='grayscale', batch_size=batch_size, shuffle=True, seed=seed, save_to_dir=augmented_masks_path):
            cnt = cnt + 1
            if cnt == images_number:
                break

        # Adding original images and masks to directory
        images = os.listdir('gdrive/MyDrive/U-Net/Resized-images/Vertebra')
        for name in images:
            image = cv2.imread(filename=os.path.join('gdrive/MyDrive/U-Net/Resized-images/Vertebra/', name), flags=cv2.IMREAD_GRAYSCALE)
            cv2.imwrite(filename=os.path.join('gdrive/MyDrive/U-Net/Augmented-images/', name), img=image)

        masks = os.listdir('gdrive/MyDrive/U-Net/Resized-masks/Vertebra')
        for name in masks:
            mask = cv2.imread(filename=os.path.join('gdrive/MyDrive/U-Net/Resized-masks/Vertebra/', name), flags=cv2.IMREAD_GRAYSCALE)
            cv2.imwrite(filename=os.path.join('gdrive/MyDrive/U-Net/Augmented-masks/', name), img=mask)
    else:
        # Displaying info 
        print('Augmented images: ' + str(len(augmented_images)))
        print('Augmented masks: ' + str(len(augmented_masks)))

  
def show_example(index=None):
    # Paths to resized images
    resized_images_path = 'gdrive/MyDrive/U-Net/Resized-images/Vertebra/'
    resized_masks_path = 'gdrive/MyDrive/U-Net/Resized-masks/Vertebra/'

    images = os.listdir(resized_images_path)
    masks = os.listdir(resized_masks_path)

    images.sort()
    masks.sort()

    if (index is None):
        index = secrets.randbelow(len(images))
 
    image_name = images[index]
    mask_name = masks[index]

    image = cv2.imread(filename=os.path.join(resized_images_path, image_name), flags=cv2.IMREAD_GRAYSCALE)
    mask = cv2.imread(filename=os.path.join(resized_masks_path, mask_name), flags=cv2.IMREAD_GRAYSCALE)

    if (image is None or mask is None):
        print('An error occured. Cannot show an example.')
    else:
        print('Example: ' + image_name)
        cv2_imshow(image)
        cv2_imshow(mask)


def mark_mask(image=None, mask=None, index=None):
    # Paths to resized images
    resized_images_path = 'gdrive/MyDrive/U-Net/Resized-images/Vertebra/'
    resized_masks_path = 'gdrive/MyDrive/U-Net/Resized-masks/Vertebra/'

    images = os.listdir(resized_images_path)
    masks = os.listdir(resized_masks_path)

    images.sort()
    masks.sort()

    if (image is None and mask is None):
        if (index is None):
            index = secrets.randbelow(len(images))
        
        image_name = images[index]
        mask_name = masks[index]
        image = cv2.imread(filename=os.path.join(resized_images_path, image_name), flags=cv2.IMREAD_GRAYSCALE)
        mask = cv2.imread(filename=os.path.join(resized_masks_path, mask_name), flags=cv2.IMREAD_GRAYSCALE)

        _,mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)
        mask = mask.astype(np.uint8)
        contours,_ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    print('Original mask before resize')
    cv2.drawContours(image, contours, -1, (0, 0, 0), 1)
    image=image*255
    cv2_imshow(image)

    desired_size = (width * 2, height * 2)
    image = cv2.resize(src=image, dsize=desired_size)
    print('Original mask after resize')
    cv2_imshow(image)

    return image


# Single block in contracting path contains 2 convolutional layers + 1 max pooling layer
def create_contracting_block(input, filters_number, kernel_size, dropout_rate):
    initializer = HeNormal(seed=300392)

    # Covolutional layers - kernel size is unrestricted, but should be odd (integer)
    convolution1 = Conv2D(filters=filters_number, kernel_size=kernel_size, padding='same', kernel_initializer=initializer)(input)
    convolution1 = BatchNormalization()(convolution1)
    convolution1 = Activation('relu')(convolution1)

    convolution2 = Conv2D(filters=filters_number, kernel_size=kernel_size, padding='same', kernel_initializer=initializer)(convolution1)
    convolution2 = BatchNormalization()(convolution2)
    convolution2 = Activation('relu')(convolution2)

    # MaxPooling layer - pool size always equal to (2,2)
    pooling = MaxPooling2D(pool_size=(2,2))(convolution2)

    # Droput layer
    dropout = Dropout(rate=dropout_rate)(pooling)

    return [convolution1, convolution2, pooling, dropout]


# Single block in expansive path contains 1 convolutional layer, deconvolutional layer & dropout layer
def create_expansive_block(input, contracting_block, filters_number, dropout_rate, first=None):
    initializer = HeNormal(seed=300392)
    kernel_size = (2,2)
    strides = (2,2)

    if first is None:
        convolution1 = Conv2D(filters=filters_number*2, kernel_size=kernel_size, padding='same', activation='relu', kernel_initializer=initializer)(input)
        convolution2 = Conv2D(filters=filters_number*2, kernel_size=kernel_size, padding='same', activation='relu', kernel_initializer=initializer)(convolution1)
    else:
        convolution2 = input

    deconvolution = Conv2DTranspose(filters=filters_number, kernel_size=kernel_size, strides=strides, padding='same')(convolution2)
    merge = concatenate(inputs=[deconvolution, contracting_block])
    dropout = Dropout(rate=dropout_rate)(merge)

    return dropout


def create_unet(get_summary=None, get_schema=None, save_weights=None):
    weights_path = 'gdrive/MyDrive/U-Net/Model/unet.h5'
    schema_path = 'gdrive/MyDrive/U-Net/Model/schema.png'

    height = 256
    width = 304

    # Model parameters
    model_optimizer = Adam(learning_rate=1e-3)
    model_input = Input((height, width, 1), name='img')
    model_initializer = HeNormal(seed=300392)
    model_metrics = [tf.keras.metrics.MeanSquaredError(), tf.keras.metrics.MeanIoU(num_classes=2)]
    
    # Hyper-parameters
    filters = 16
    kernel_size = (3,3)
    dropout_rate = 0.75

    # Creating contracting path - 4 blocks
    [_, conv1, _, drop1] = create_contracting_block(model_input, filters, kernel_size, dropout_rate)
    [_, conv2, _, drop2] = create_contracting_block(drop1, filters*2, kernel_size, dropout_rate)
    [_, conv3, _, drop3] = create_contracting_block(drop2, filters*4, kernel_size, dropout_rate)
    [_, conv4, _, drop4] = create_contracting_block(drop3, filters*8, kernel_size, dropout_rate)

    # Creating central block with two convolutional layers
    center1 = Conv2D(filters*16, kernel_size, padding='same', activation='relu', kernel_initializer=model_initializer)(drop4)
    center2 = Conv2D(filters*16, kernel_size, padding='same', activation='relu', kernel_initializer=model_initializer)(center1)

    # Creating expansive path - 4 blocks
    drop6 = create_expansive_block(center2, conv4, filters*8, dropout_rate, first=True)
    drop7 = create_expansive_block(drop6, conv3, filters*4, dropout_rate)
    drop8 = create_expansive_block(drop7, conv2, filters*2, dropout_rate)
    drop9 = create_expansive_block(drop8, conv1, filters, dropout_rate)

    # Creating output block with two convolutional layers
    output1 = Conv2D(filters, kernel_size, padding='same', activation='relu', kernel_initializer=model_initializer)(drop9)
    output2 = Conv2D(filters, kernel_size, padding='same', activation='relu', kernel_initializer=model_initializer)(output1)

    output = Conv2D(filters=1, kernel_size=1, activation='sigmoid')(output2)

    # Creating a model
    model = Model(inputs=[model_input], outputs=[output])

    model.compile(optimizer=model_optimizer, loss="binary_crossentropy", metrics=model_metrics)

    if get_summary:
        model.summary()
    if get_schema:
        plot_model(model=model, to_file=schema_path)

    if save_weights:
        print('Saving weights to file...')
        model.save(weights_path)
    else: 
        print('Loading weights from file...')
        model.load_weights(weights_path)
        
    return model


def train(model, images, masks, valid_images, valid_masks, batch_size, epochs, show_history=None):
    weights_path = 'gdrive/MyDrive/U-Net/Model/unet.h5'

    if show_history is None:
        show_history = False

    model_checkpoint = tf.keras.callbacks.ModelCheckpoint(
        filepath=weights_path,
        save_weights_only=True,
        monitor='loss',
        verbose=True,
        mode='min',
        save_best_only=True)
    
    model_callbacks = [
        EarlyStopping(patience=5, verbose=1),
        ReduceLROnPlateau(factor=0.001, patience=3, min_lr=0.0001, verbose=1),
        model_checkpoint
    ]

    model.load_weights(weights_path)
    results = model.fit(x=images, y=masks, batch_size=batch_size, epochs=epochs, callbacks=model_callbacks, shuffle=True, validation_data=(valid_images, valid_masks))

    if (show_history):
        plot_metrics_history(results=results)

    return results


def plot_metrics_history(results):
    print('AVAILABLE METRICS:')
    print(results.history.keys())
    print(type(results.history.keys))

    plt.figure()
    plt.plot(results.history['loss'], label='train')
    plt.plot(results.history['val_loss'], label='validation')
    plt.legend()
    plt.title('LOSS')
    plt.show()

    plt.figure()
    plt.plot(results.history['mean_squared_error'], label='train')
    plt.plot(results.history['val_mean_squared_error'], label='validation')
    plt.legend()
    plt.title('MEAN SQUARED ERROR')
    plt.show()


def predict(model, test_dataset, show=None):
    if show is None:
        show = False

    weights_path = 'gdrive/MyDrive/U-Net/Model/unet.h5'

    model.load_weights(weights_path)
    predicted = model.predict(x=test_dataset, verbose=1)

    if (show):
        for image in predicted:
            print('after rescale')
            image=image*255
            cv2_imshow(image)
            print('after threshold')
            _,image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)
            cv2_imshow(image)

    return predicted


def mark_predictions(image, prediction):
    image = image * 255
    prediction = prediction * 255

    _,prediction = cv2.threshold(prediction, 127, 255, cv2.THRESH_BINARY)
    cv2_imshow(prediction)
    prediction = prediction.astype(np.uint8)
    contours,_ = cv2.findContours(prediction, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    cv2.drawContours(image, contours, -1, (0,255,0), 1)
    cv2_imshow(image)
  
    return [image, prediction]


#-------------------------------------------------------------------------

height = 256
width = 304

epochs = 50
batch_size = 5

weights_path = 'gdrive/MyDrive/U-Net/Model/unet.h5'

[images,masks] = load_data()
resize_data(images_names=images, masks_names=masks, new_h=height, new_w=width)

del images[:]
del masks[:]

show_example()
mark_mask()

augment_data(h=height, w=width)
[train_images, test_images, train_masks, test_masks] = prepare_data(dataset_size=1000, split_size=None)

#-------------------------------------------------------------------------

u_net = create_unet(get_schema=True, save_weights=False)
results = train(model=u_net, images=train_images, masks=train_masks, valid_images=test_images, valid_masks=test_masks, batch_size=batch_size, epochs=epochs, show_history=True)
predicted = predict(model=u_net, test_dataset=test_images, show=False)

desired_size = (width * 2, height * 2)

for i in range(0, len(predicted)):
    [im, pred] = mark_predictions(test_images[i], predicted[i])

    test = test_images[i] * 255
    test = cv2.resize(src=test, dsize=desired_size)
    cv2_imshow(test)

    test_masks[i] = test_masks[i] * 255
    cv2_imshow(test_masks[i])
    mark_mask(test_images[i], test_masks[i])

    im = cv2.resize(src=im, dsize=desired_size)
    cv2_imshow(im)

    pred = cv2.resize(src=pred, dsize=desired_size)
    cv2_imshow(pred)
